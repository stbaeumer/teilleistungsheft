﻿using System;
using System.Collections.Generic;

namespace teilleistungsheft
{
    public class Quartal
    {
        public DateTime Von;
        public DateTime Bis;
        public string Id { get; private set; }
        public string VideoSoleiEintragen { get; private set; }
        public string VideoNoteEintragen { get; private set; }
        public List<string> Solei { get; internal set; }
        public List<string> SchriftlicheArbeiten { get; private set; }
        public string Text { get; private set; }
        public string Betreff { get; private set; }

        public Quartal(
            string id, 
            DateTime von, 
            DateTime bis, 
            string videoNoteEintragen, 
            string videoSoleiEintragen, 
            List<string> prüfungsarten, 
            List<string> schriftlicheArbeiten, 
            string betreff,
            string text)
        {
            this.Id = id;
            this.Von = von;
            this.Bis = bis;
            this.VideoNoteEintragen = videoNoteEintragen;
            this.VideoSoleiEintragen = videoSoleiEintragen;
            this.Solei = prüfungsarten;
            this.SchriftlicheArbeiten = schriftlicheArbeiten;
            this.Betreff = betreff;
            this.Text = text;
        }
    }
}