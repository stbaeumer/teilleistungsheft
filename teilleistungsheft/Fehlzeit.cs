﻿namespace teilleistungsheft
{
    public class Fehlzeit
    {
        public int StudentId { get; internal set; }
        public string Name { get; internal set; }
        public string Klasse { get; internal set; }
        public int AbsenceHoursNotExcused { get; internal set; }
        public int AbsenceHours { get; internal set; }
        public string Vorname { get; internal set; }
    }
}