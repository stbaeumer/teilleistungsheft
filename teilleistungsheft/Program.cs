﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace teilleistungsheft
{
    class Program
    {
        public const string ConnectionStringAtlantis = @"Dsn=Atlantis9;uid=DBA";
        public const string ConnectionStringUntis = @"Provider = Microsoft.Jet.OLEDB.4.0; Data Source=M:\\Data\\gpUntis.mdb;";

        static void Main(string[] args)
        {
            Global.Fehlers = new Fehlers();            
            Global.Schulnummer = 177659;
            
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

            try
            {                
                string inputAbwesenheitenCsv = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\AbsenceTimesTotal.csv";
                string outputPfad = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\output.txt";
                string marksPerLesson = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\MarksPerLesson.csv";

                Console.WriteLine(" Teilleistungsheft | Published under the terms of GPLv3 | Stefan Bäumer | Version 20191018");
                Console.WriteLine("==========================================================================================");
                Console.WriteLine("");
                Console.WriteLine("Schulnummer:   " + Global.Schulnummer);
                Console.WriteLine("Prüfungsarten: Klassenarbeit (Anlage A-C), Klausur (Anlage D), Q1-SoLei");
                Console.WriteLine("Schriftliche Arbeiten: Klassenarbeit (Anlage A-C), Klausur (Anlage D)");
                Console.WriteLine("");

                string quartal = "";

                do
                {
                    Console.Write("Für welches Quartal oder Zeugnis sollen die Noteneinträge geprüft werden? [1,2,3,4,HZ,JZ] ENTER: ");
                    quartal = Console.ReadLine().ToString().ToUpper();

                } while (!(new List<string>() { "1", "2", "3", "4", "HZ", "JZ" }).Contains(quartal));
                
                int sj = (DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1);
                string aktSjUntis = sj.ToString() + (sj + 1);
                string aktSjAtlantis = sj.ToString() + "/" + (sj + 1 - 2000);
                
                Studentgroups studentgroups = new Studentgroups();
                ExportLessons exportlessons = new ExportLessons();
                Leistungen leistungen = new Leistungen(marksPerLesson);
                Fehlzeiten fehlzeiten = new Fehlzeiten();
                Fachs fachs = new Fachs(ConnectionStringUntis, aktSjUntis);
                Examinations examinations = new Examinations(fachs);
                Periodes periodes = new Periodes(ConnectionStringUntis, aktSjUntis);
                Klasses klasses = new Klasses(aktSjUntis, ConnectionStringUntis, periodes.AktuellePeriode);
                Schuelers schuelers = new Schuelers(ConnectionStringAtlantis);
                Lehrers lehrers = new Lehrers(aktSjUntis, ConnectionStringUntis, periodes);
                Quartal aktuellesQuartal = new Quartals().GetAktuellesQuartal(quartal);

                exportlessons.PrüfeTeilleistungen(
                    aktuellesQuartal, 
                    periodes, 
                    schuelers, 
                    klasses, 
                    studentgroups, 
                    leistungen, 
                    examinations,
                    fehlzeiten);

                Global.Fehlers.DateiAusgabe(outputPfad);
                
                Global.Fehlers.Senden(lehrers, aktuellesQuartal);

                Console.WriteLine("Verarbeitung beendet.");
                Console.ReadKey();                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Heiliger Bimbam! Es ist etwas schiefgelaufen! Die Verarbeitung wird gestoppt.");
                Console.WriteLine("");
                Console.WriteLine(ex);
                Console.ReadKey();
                Environment.Exit(0);
            }
        }
        
        private static void RenderNotenexportCsv(string inputNotenCsv)
        {
            Console.WriteLine("Die Datei " + inputNotenCsv + " existiert nicht.");
            Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
            Console.WriteLine(" 1. Klassenbuch > Berichte klicken");
            Console.WriteLine(" 2. Alle Klassen auswählen");
            Console.WriteLine(" 3. Unter \"Noten\" die Prüfungsart (z.B. Halbjahreszeugnis) auswählen");
            Console.WriteLine(" 4. Hinter \"Noten pro Schüler\" auf CSV klicken.");
            Console.WriteLine(" 5. Die Datei \"MarksPerLesson.csv\" auf dem Desktop speichern.");
            Console.WriteLine("ENTER beendet das Programm.");
            Console.ReadKey();
            Environment.Exit(0);
        }
    }
}
