﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace teilleistungsheft
{
    public class ExportLessons : List<ExportLesson>
    {
        public ExportLessons()
        {
            string datei = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ExportLessons.csv";

            if (!File.Exists(datei))
            {
                Console.WriteLine("Die Datei " + datei + " existiert nicht.");
                Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
                Console.WriteLine("1. sich als admin anmelden");
                Console.WriteLine("2. auf Administration > Export klicken");
                Console.WriteLine("3. Unterricht als CSV exportieren nach " + datei);
                Console.WriteLine("ENTER beendet das Programm.");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
            {
                if (System.IO.File.GetLastWriteTime(datei).Date != DateTime.Now.Date)
                {
                    Console.WriteLine("Die Datei " + datei + " ist nicht von heute.");
                    Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
                    Console.WriteLine("1. sich als admin anmelden");
                    Console.WriteLine("2. auf Administration > Export klicken");
                    Console.WriteLine("3. Unterricht als CSV exportieren nach " + datei);
                    Console.WriteLine("ENTER beendet das Programm.");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }
            using (StreamReader reader = new StreamReader(datei))
            {
                Console.Write("Unterrichte aus Webuntis ".PadRight(70, '.'));

                while (true)
                {
                    string line = reader.ReadLine();
                    try
                    {
                        ExportLesson exportLesson = new ExportLesson();
                        var x = line.Split('\t');
                        exportLesson.LessonId = Convert.ToInt32(x[0]);
                        exportLesson.LessonNumber = Convert.ToInt32(x[1]) / 100;
                        exportLesson.Subject = x[2];
                        exportLesson.Lehrkraft = x[3];
                        exportLesson.Klassen = x[4];
                        exportLesson.Studentgroup = x[5];
                        exportLesson.Periods = x[6];
                        exportLesson.Startdate = DateTime.ParseExact(x[7], "dd.MM.yyyy", CultureInfo.InvariantCulture);
                        exportLesson.EndDate = DateTime.ParseExact(x[8], "dd.MM.yyyy", CultureInfo.InvariantCulture);
                        exportLesson.Room = x[9];
                        exportLesson.Foreignkey = x[9];
                        this.Add(exportLesson);
                    }
                    catch (Exception)
                    {
                    }

                    if (line == null)
                    {
                        break;
                    }
                }

                Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));
            }
        }

        internal void PrüfeTeilleistungen(Quartal aktuellesQuartal, Periodes periodes, Schuelers schuelers, Klasses klasses, Studentgroups studentgroups, Leistungen leistungen, Examinations examinations, Fehlzeiten fehlzeiten)
        {
            string fach = "";
            string klassen = "";

            foreach (var unterricht in this.OrderBy(x => x.Klassen).ThenBy(x => x.Subject).ThenByDescending(x => x.LessonId))
            {
                var bereichsleiter = (from k in klasses where k.NameUntis == unterricht.Klassen.Split('~')[0] select k.Abteilung).FirstOrDefault();
                var klassenleiter = (from k in klasses where k.NameUntis == unterricht.Klassen.Split('~')[0] select k.KlassenlehrerKürzel).FirstOrDefault();
                
                if (unterricht.IstRelevant(aktuellesQuartal, klasses))
                {
                    // Wenn ein Unterricht in einer Klasse 2x auftaucht, dann wird nur der zuerst angelegte Unterricht berücksichtigt.

                    if (!(fach == unterricht.Subject && klassen == unterricht.Klassen))
                    {
                        fach = unterricht.Subject;
                        klassen = unterricht.Klassen;

                        if (unterricht.Lehrkraft == "BM")
                        {
                            Leistungen relevanteLeistungen = new Leistungen();

                            if (unterricht.IstKlassenunterricht())
                            {
                                foreach (var klasse in unterricht.Klassen.Split('~'))
                                {
                                    Schuelers schuelerDieserKlasse = new Schuelers();
                                    schuelerDieserKlasse.AddRange(from s in schuelers where s.Grade == klasse select s);

                                    int anzahlSchüler = schuelerDieserKlasse.Count();
                                    
                                    string schriftlicheArbeit = examinations.SchriftlicheArbeit(unterricht, aktuellesQuartal);

                                    bool istSchriftlicheArbeitAngelegt = schriftlicheArbeit == "" ? false : true;

                                    string sonstigeLeistung = examinations.SonstigeLeistung(unterricht, aktuellesQuartal);

                                    bool istSonstigeLeistungAngelegt = sonstigeLeistung == "" ? false : true;

                                    List<Leistung> bewerteteSchriftlicheArbeiten = leistungen.BerwerteteSchriftlicheArbeiten(unterricht, aktuellesQuartal);

                                    int anzahlBewerteteSchriftlicheArbeiten = bewerteteSchriftlicheArbeiten.Count;

                                    bool istSchriftlicheArbeitBewertet = anzahlBewerteteSchriftlicheArbeiten > 0 ? true : false;

                                    List<Leistung> korrespondierendeSonstigeLeistungen = leistungen.KorrespondierendeSonstigeLeistungen(unterricht, aktuellesQuartal);

                                    int anzahlKorrespondierendeSonstigeLeistungen = korrespondierendeSonstigeLeistungen.Count();

                                    bool istKorrespondierendeSonstigeLeistungenAngelegt = anzahlKorrespondierendeSonstigeLeistungen > 0 ? true : false;

                                    List<Leistung> bewerteteSonstigeLeistungen = leistungen.BerwerteteSonstigeLeistungen(unterricht, aktuellesQuartal);

                                    int anzahlBewerteteSonstigeLeistungen = bewerteteSonstigeLeistungen.Count();

                                    bool istSonstigeLeistungBewertet = anzahlBewerteteSonstigeLeistungen > 0 ? true : false;

                                    List <Schueler> schuelerMitHohenFehlzeiten = schuelerDieserKlasse.GetSchuelerMitHohenFehlzeiten(fehlzeiten);

                                    bool istKlasseMitHohenFehlzeiten = schuelerMitHohenFehlzeiten.Count > 0 ? true : false;
                                    
                                    // Wenn nichts angelegt ist.

                                    if (!istSchriftlicheArbeitAngelegt && !istSonstigeLeistungAngelegt)
                                    {
                                        Global.Fehlers.Add(
                                                    new Fehler(
                                                        unterricht.Lehrkraft,
                                                        "",
                                                        "",
                                                        klasse,
                                                        unterricht.Subject,
                                                        "",
                                                        "Es wird unterschieden zwischen schriftlichen und nicht-schriftlichen Fächern:<ul><li>Wenn das Fach " + unterricht.Subject + " ein <b>schriftliches Fach</b> ist, muss eine Prüfung der Prüfungsart <b>" + (klasse.StartsWith("G") ? aktuellesQuartal.SchriftlicheArbeiten[0] : aktuellesQuartal.SchriftlicheArbeiten[1]) + "</b> angelegt werden. Zu jeder schriftlichen Note tragen Sie in die <b>Bemerkung</b> die <b>SoLei-Note</b> ein. Das Video zeigt wie es geht.</li> <li>Wenn das Fach " + unterricht.Subject + " <b> kein schriftliches Fach</b> ist, dann müssen Sie eine Prüfung der Prüfungart <b>" + aktuellesQuartal.Solei[0] + "</b> anlegen und dort lediglich die <b>" + aktuellesQuartal.Solei[0] + "-Note</b> eintragen. Auch hierzu gibt es ein Video, das zeigt wie es geht.</li></ul>"
                                                    )
                                                );
                                    }

                                    // Wenn eine schriftliche Prüfung angelegt ist, aber sonst nichts

                                    if (istSchriftlicheArbeitAngelegt && !istSchriftlicheArbeitBewertet && !istKorrespondierendeSonstigeLeistungenAngelegt && !istSonstigeLeistungAngelegt)
                                    {
                                        Global.Fehlers.Add(
                                                    new Fehler(
                                                        unterricht.Lehrkraft,
                                                        "",
                                                        "",
                                                        klasse,
                                                        unterricht.Subject,
                                                        "",
                                                        "Sie haben erfolgreich eine <b>" + schriftlicheArbeit + "</b> im Fach " + unterricht.Subject + " angelegt. <ul><li>Als nächstes müssen sie die  <b>" + schriftlicheArbeit + "</b>-Noten dort eintragen.</li><li> Zu jeder  " + schriftlicheArbeit + "-Note tragen Sie zusätzlich eine <b>SoLei</b>-Note in die <b>Bemerkung</b> dahinter ein. Siehe Video.</li></ul>"
                                                    )
                                                );                                        
                                    }

                                    // Wenn eine schriftliche und eine Solei-Prüfung außerhab der Gym angelegt wurde.
                                    
                                    if (!klasse.StartsWith("G") && istSchriftlicheArbeitAngelegt && istSonstigeLeistungAngelegt)
                                    {
                                        Global.Fehlers.Add(
                                                    new Fehler(
                                                        unterricht.Lehrkraft,
                                                        "",
                                                        "",
                                                        klasse,
                                                        unterricht.Subject,
                                                        "",
                                                        "Sie haben erfolgreich eine <b>" + schriftlicheArbeit + "<b> im Fach " + unterricht.Subject + " angelegt. Zudem haben Sie eine Prüfungsart <b>" + sonstigeLeistung + "</b> angelegt.<ul><li> In einem schriftlichen Fach ist es ausreichend nur eine <b>" + schriftlicheArbeit + "</b> anzulegen. Dort tragen Sie die <b>" + schriftlicheArbeit + "</b>-Noten ein.</li><li>In die Bemerkung dahinter tragen Sie die Note für die Sonstige Leistung ein. Siehe Video.</li></ul>"
                                                    )
                                                );                                        
                                    }

                                    // Wenn eine schriftliche und eine Solei-Prüfung in der Gym angelegt wurde.

                                    if (klasse.StartsWith("G") && istSchriftlicheArbeitAngelegt && istSonstigeLeistungAngelegt)
                                    {
                                        Global.Fehlers.Add(
                                                   new Fehler(
                                                       unterricht.Lehrkraft,
                                                       "",
                                                       "",
                                                       klasse,
                                                       unterricht.Subject,
                                                       "",
                                                       "Sie haben erfolgreich eine <b>" + schriftlicheArbeit + "</b> im Fach " + unterricht.Subject + " angelegt. Zudem haben Sie eine Prüfungsart <b>" + sonstigeLeistung + "</b> angelegt. <ul><li> Die Klausurnoten sind in der <b>" + schriftlicheArbeit + "</b> einzugeben. Hinter jeder Klausurnote ist Platz für eine <b>Bemerkung</b>. Dort tragen Sie die Note für die Sonstige Leistung ein.</li><li> Für Testschreiber muss eine eigene Prüfungsart namens <b>" + aktuellesQuartal.Solei[0] + "</b> angelegt werden. Dort tragen Sie die Sonstige Leistung der Testschreiber ein. Wenn es im Fach " + unterricht.Subject + " keine Testschreiber gibt, ist es ausreichend die Prüfungsart <b>" + schriftlicheArbeit + "</b> anzulegen und dort beide Noten einzutragen. Siehe Video.</li></ul>"
                                                   )
                                               );                                        
                                    }

                                    // Wenn außerhalb der Gym im schriftlichen Fach nicht alle SuS eine Klausurnote haben

                                    if (!klasse.StartsWith("G") && istSchriftlicheArbeitAngelegt && anzahlSchüler > anzahlBewerteteSchriftlicheArbeiten)
                                    {
                                        Global.Fehlers.Add(
                                                   new Fehler(
                                                       unterricht.Lehrkraft,
                                                       "",
                                                       "",
                                                       klasse,
                                                       unterricht.Subject,
                                                       "",
                                                       "Sie haben erfolgreich eine <b>" + schriftlicheArbeit + "</b> im Fach " + unterricht.Subject + " angelegt. Es sind auch schon " + anzahlBewerteteSchriftlicheArbeiten + " von " + anzahlSchüler + " <b>" + schriftlicheArbeit + "</b>-Noten eintragen. <ul><li> Es fehlen aber noch " + (anzahlSchüler - anzahlBewerteteSchriftlicheArbeiten) + " Noten. Das Video zeigt, wie es geht.</li></ul>"
                                                   )
                                               );                                        
                                    }

                                    // Wenn außerhalb der Gym in einem mündlichen Fach nicht alle Noten erfasst sind.

                                    if (!klasse.StartsWith("G") && !istSchriftlicheArbeitAngelegt && istSonstigeLeistungAngelegt && anzahlSchüler > anzahlBewerteteSonstigeLeistungen)
                                    {
                                        Global.Fehlers.Add(
                                                  new Fehler(
                                                      unterricht.Lehrkraft,
                                                      "",
                                                      "",
                                                      klasse,
                                                      unterricht.Subject,
                                                      "",
                                                      "Sie haben erfolgreich die Prüfungsart <b>" + aktuellesQuartal.SchriftlicheArbeiten[1] + "</b> im Fach " + unterricht.Subject + " angelegt. Da keine Prüfungsart " + aktuellesQuartal.SchriftlicheArbeiten[0] + " oder " + aktuellesQuartal.SchriftlicheArbeiten[1] + " angelegt wurde, handelt es sich offensichtlich um ein <b>nicht-schriftliches</b> Fach. <ul><li>Es fehlen aber noch " + (schuelerDieserKlasse.Count - bewerteteSonstigeLeistungen.Count) + " Noten. Das Video zeigt, wie es geht.</li></ul>"
                                                  )
                                              );                                        
                                    }

                                    // Wenn hohe Fehlzeiten bei einzelnen SuS entstanden sind.

                                    if (false)
                                    {
                                        if (istKlasseMitHohenFehlzeiten)
                                        {
                                            string namen = "";

                                            foreach (var item in schuelerMitHohenFehlzeiten)
                                            {
                                                namen += item + ",";
                                                Global.Fehlers.Add(
                                                    new Fehler(
                                                        klassenleiter,
                                                        item.Name + "," + item.Firstname,
                                                        "",
                                                        klasse,
                                                        unterricht.Subject,
                                                        "",
                                                        "In Ihrer Klasse hat " + item.Firstname + " " + item.Name + " auffallend hohe, unentschuldigte Fehlzeiten."
                                                    )
                                                );
                                            }



                                            /*
                                             * Im Ermessen der Lehrkraft:
                                            Ab 25% Fehlzeit kann im Einzelfall eine Nichtbewertung infrage kommen. 
                                            Ab 50% Fehlzeit ist eine Bewertung in der Regel nicht möglich.
                                            Ein Rückgriff auf Noten aus dem Vorjahr ist unzulässig.                                     
                                         */


                                        }
                                    }
                                }
                            }
                            if (unterricht.IstKursunterricht())
                            {/*
                                studentgroups.AufLeistungsdatensätzePrüfen(
                                    relevanteLeistungen,
                                    unterricht,
                                    klasses,
                                    examinations,
                                    aktuellesQuartal,
                                    bereichsleiter,
                                    klassenleiter);*/
                            }
                        }
                    }
                }
            }
        }
    }
}