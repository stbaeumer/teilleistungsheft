﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace teilleistungsheft
{
    public class Lehrers : List<Lehrer>
    {
        public Lehrers()
        {
        }

        public Lehrers(string aktSj, string connectionString, Periodes periodes)
        {

            Console.Write("Lehrer aus Atlantis ".PadRight(70, '.'));

            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionString))
            {
                try
                {
                    string queryString = @"SELECT DISTINCT 
Teacher.Teacher_ID, 
Teacher.Name, 
Teacher.Longname, 
Teacher.FirstName,
Teacher.Email,
Teacher.PlannedWeek
FROM Teacher 
WHERE (((SCHOOLYEAR_ID)= " + aktSj + ") AND  ((TERM_ID)=" + periodes.Count + ") AND ((Teacher.SCHOOL_ID)=" + Global.Schulnummer + ") AND (((Teacher.Deleted)=No))) ORDER BY Teacher.Name;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        Lehrer lehrer = new Lehrer()
                        {
                            IdUntis = oleDbDataReader.GetInt32(0),
                            Kürzel = Global.SafeGetString(oleDbDataReader, 1),
                            Nachname = Global.SafeGetString(oleDbDataReader, 2),
                            Vorname = Global.SafeGetString(oleDbDataReader, 3),
                            Mail = Global.SafeGetString(oleDbDataReader, 4)
                        };

                        if (!lehrer.Mail.EndsWith("@berufskolleg-borken.de") && lehrer.Kürzel != "LAT" && lehrer.Kürzel != "?")
                            Global.MailSenden(lehrer, "Teilleistung Fehlermeldung", "Der Lehrer " + lehrer.Kürzel + " hat keine Mail-Adresse in Untis. Bitte in Untis eintragen.");

                        this.Add(lehrer);
                    };

                    Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }

        internal void Senden(string v, Quartal quartal)
        {
            foreach (var lehrer in this)
            {
                Console.Write(("Mail an " + lehrer.Kürzel + " (" + v + ") ").PadRight(50, '.'));

                lehrer.Mailen(v, quartal);
            }
        }
    }
}