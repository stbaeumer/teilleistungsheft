﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace teilleistungsheft
{
    public class ExportLesson
    {
        public int LessonId { get; internal set; }
        public int LessonNumber { get; internal set; }
        public string Subject { get; internal set; }
        public string Lehrkraft { get; internal set; }
        public string Klassen { get; internal set; }
        public string Studentgroup { get; internal set; }
        public string Periods { get; internal set; }
        public DateTime Startdate { get; internal set; }
        public DateTime EndDate { get; internal set; }
        public string Room { get; internal set; }
        public string Foreignkey { get; internal set; }

        internal bool IstRelevant(Quartal aktuellesQuartal, Klasses klasses)
        {
            if (TeilleistungGefordert(klasses, aktuellesQuartal.Id))
            {
                if (this.Subject != "")
                {
                    // Relevante Unterrichte müssen länger als über eine Zeitspanne von 28 Tagen unterricht worden sein

                    if (EndDate >= Startdate.AddDays(28))
                    {
                        // Die Periode des Unterrichts muss mindestens teilweise im Quartal liegen

                        if (Startdate <= aktuellesQuartal.Bis && EndDate > aktuellesQuartal.Von)
                        {
                            return true;
                        }
                    }
                }
            }
                        
            return false;
        }

        private bool TeilleistungGefordert(Klasses klasses, string quartal)
        {
            // Jahreszeugnisse gibt es für alle Unterrichte

            if (quartal == "JZ")
            {
                return true;
            }

            var jahrgang = (from k in klasses where k.NameUntis == this.Klassen.Split('~')[0] select k.SchildJahrgang).FirstOrDefault();

            // Dokumentation von Teilleistungen und HZ in Berufsschulklassen nicht im 1. und 3. Quartal.

            if ((new List<string>() { "BS-3J-01", "BS-3J-02", "BS-35J-01", "BS-35J-02", "BS-35J-03", "Block-01", "Block-02" }).Contains(jahrgang))
            {
                if (quartal == "HZ")
                {
                    return false;
                }
                if (Convert.ToInt32(quartal) % 2 != 0)
                {
                    return false;
                }                
            }

            if (jahrgang == "" || Klassen == "")
            {
                return false;
            }

            return true;
        }

        internal bool IstKlassenunterricht()
        {
            return Studentgroup == "" ? true : false;
        }

        internal bool IstKursunterricht()
        {
            return Studentgroup == "" ? false : true;
        }
    }
}