﻿namespace teilleistungsheft
{
    public class Fehler
    {        
        public string SchuelerName;
        public string Klasse;
        public string Prüfungsart;
        public string Schülergruppe { get; private set; }
        public string Bereichsleitung { get; private set; }
        public string Klassenleitung { get; private set; }
        public string Adressat { get; private set; }
        public string Fach { get; private set; }
        public string Fehlerbeschreibung { get; private set; }

        public Fehler(string adressat, string schuelerName, string schülergruppe, string klasse, string fach, string prüfungsart, string fehlerbeschreibung)
        {
            this.Adressat = adressat;
            this.SchuelerName = schuelerName;
            this.Schülergruppe = schülergruppe;
            this.Klasse = klasse;
            this.Fach = fach;
            this.Prüfungsart = prüfungsart;
            this.Fehlerbeschreibung = fehlerbeschreibung;
        }
    }
}