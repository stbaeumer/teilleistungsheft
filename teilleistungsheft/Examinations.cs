﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace teilleistungsheft
{
    public class Examinations : List<Examination>
    {
        public Examinations(Fachs fachs)
        {
            string datei = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Examinations.csv";

            if (!File.Exists(datei))
            {
                Console.WriteLine("Die Datei " + datei + " existiert nicht.");
                Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
                Console.WriteLine("1. sich als admin anmelden");
                Console.WriteLine("2. auf Klassenbuch > Berichte klicken");
                Console.WriteLine("3. Prüfungen als CSV exportieren nach " + datei);
                Console.WriteLine("ENTER beendet das Programm.");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
            {
                if (System.IO.File.GetLastWriteTime(datei).Date != DateTime.Now.Date)
                {
                    Console.WriteLine("Die Datei " + datei + " existiert nicht.");
                    Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
                    Console.WriteLine("1. sich als admin anmelden");
                    Console.WriteLine("2. auf Klassenbuch > Berichte klicken");
                    Console.WriteLine("3. Prüfungen als CSV exportieren nach " + datei);
                    Console.WriteLine("ENTER beendet das Programm.");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }
            using (StreamReader reader = new StreamReader(datei))
            {
                Console.Write("Prüfungen aus Webuntis ".PadRight(70, '.'));

                while (true)
                {
                    string line = reader.ReadLine();
                    try
                    {
                        Examination examination = new Examination();
                        var x = line.Split('\t');
                        examination.Prüfungsart = x[0];                        
                        examination.Name = x[1];
                        examination.Klasse = x[2];
                        examination.Note = x[3];
                        examination.Datum = DateTime.ParseExact(x[4], "dd.MM.yyyy", CultureInfo.InvariantCulture);
                        examination.Von = DateTime.ParseExact(x[5], "hh:mm", CultureInfo.InvariantCulture);
                        examination.Bis = DateTime.ParseExact(x[6], "hh:mm", CultureInfo.InvariantCulture);
                        examination.Rückgabe = x[7] == "" ? new DateTime() : DateTime.ParseExact(x[7], "dd.MM.yyyy", CultureInfo.InvariantCulture);                        
                        examination.Fach = (from f in fachs where f.Langname == x[8] select f.KürzelUntis).FirstOrDefault();
                        examination.Lehrer = x[10];
                        examination.Foreignkey = x[9] == "" ? 0 : Convert.ToInt32(x[9]);
                        this.Add(examination);
                    }
                    catch (Exception)
                    {
                    }

                    if (line == null)
                    {
                        break;
                    }
                }

                Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));
            }
        }

        internal string SonstigeLeistung(ExportLesson unterricht, Quartal aktuellesQuartal)
        {
            foreach (var e in this)
            {                
                if (unterricht.Klassen.Contains(e.Klasse))
                {
                    if (unterricht.Subject == e.Fach)
                    {
                        if (aktuellesQuartal.Von <= e.Datum && e.Datum <= aktuellesQuartal.Bis)
                        {
                            if (aktuellesQuartal.Solei.Contains(e.Prüfungsart))
                            {
                                return e.Prüfungsart;
                            }
                        }
                    }
                }
            }
            return "";
        }

        internal string SchriftlicheArbeit(ExportLesson unterricht, Quartal aktuellesQuartal)
        {
            foreach (var e in this)
            {
                if (unterricht.Klassen.Contains(e.Klasse))
                {
                    if (unterricht.Subject == e.Fach)
                    {
                        if (aktuellesQuartal.Von <= e.Datum && e.Datum <= aktuellesQuartal.Bis)
                        {
                            if (aktuellesQuartal.SchriftlicheArbeiten.Contains(e.Prüfungsart))
                            {
                                return e.Prüfungsart;
                            }
                        }
                    }
                }
            }
            return "";
        }
        
        internal bool IstSchriftlicheArbeitFürSchülerAngelegt(ExportLesson unterricht, Quartal aktuellesQuartal, Studentgroup kursteilnehmer)
        {
            foreach (var e in this)
            {
                if (unterricht.Klassen.Contains(e.Klasse))
                {
                    if (unterricht.Subject == e.Fach)
                    {
                        if (e.Foreignkey == kursteilnehmer.StudentId)
                        {
                            if (aktuellesQuartal.Von <= e.Datum && e.Datum <= aktuellesQuartal.Bis)
                            {
                                if (aktuellesQuartal.SchriftlicheArbeiten.Contains(e.Prüfungsart))
                                {
                                    return true;
                                }
                            }
                        }                       
                    }
                }
            }
            return false;
        }
    }
}