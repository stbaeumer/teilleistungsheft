﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace teilleistungsheft
{
    public class Klasses : List<Klasse>
    {
        public Klasses(string aktSjUntis, string connectionStringUntis, int aktuellePeriode)
        {
            Console.Write("Klassen aus Webuntis ".PadRight(70, '.'));

            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionStringUntis))
            {
                string queryString = @"SELECT DISTINCT 
Class.Name, 
Class.Class_ID, 
Class.Longname, 
Teacher.Name, 
Class.ClassLevel,
Class.PERIODS_TABLE_ID,
Department.Name
FROM (Class LEFT JOIN Department ON Class.DEPARTMENT_ID = Department.DEPARTMENT_ID) LEFT JOIN Teacher ON Class.TEACHER_ID = Teacher.TEACHER_ID
WHERE (((Class.SCHOOL_ID)=" + Global.Schulnummer + ") AND ((Class.SCHOOLYEAR_ID)=" + aktSjUntis + @"))
ORDER BY Class.CLASS_ID DESC;";
                
                OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                oleDbConnection.Open();
                OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();
                
                while (oleDbDataReader.Read())
                {
                    Klasse klasse = new Klasse()
                    {               
                        NameUntis = Global.SafeGetString(oleDbDataReader, 0),
                        IdUntis = oleDbDataReader.GetInt32(1),
                        LangNameUntis = Global.SafeGetString(oleDbDataReader, 2),
                        KlassenlehrerKürzel = ((Global.SafeGetString(oleDbDataReader, 3)).Split(','))[0],
                        SchildJahrgang = Global.SafeGetString(oleDbDataReader, 4),                        
                        Abteilung = Global.SafeGetString(oleDbDataReader, 6)
                    };                    
                    this.Add(klasse);
                };

                oleDbDataReader.Close();
                oleDbConnection.Close();
                Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));
            }
        }        
    }
}