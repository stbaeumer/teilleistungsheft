﻿using System;
using System.Collections.Generic;
using System.IO;

namespace teilleistungsheft
{
    public class Fehlzeiten : List<Fehlzeit>
    {
        public Fehlzeiten()
        {
            string datei = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\AbsenceTimesTotal.csv";

            if (!File.Exists(datei))
            {
                Console.WriteLine("Die Datei " + datei + " existiert nicht.");
                Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
                Console.WriteLine("1. sich als admin anmelden");
                Console.WriteLine("2. auf Administration > Export klicken");
                Console.WriteLine("3. Gesamtfehlzeiten als CSV exportieren nach " + datei);
                Console.WriteLine("ENTER beendet das Programm.");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
            {
                if (System.IO.File.GetLastWriteTime(datei).Date != DateTime.Now.Date)
                {
                    Console.WriteLine("Die Datei " + datei + " ist nicht von heute.");
                    Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
                    Console.WriteLine("1. sich als admin anmelden");
                    Console.WriteLine("2. auf Administration > Export klicken");
                    Console.WriteLine("3. Gesamtfehlzeiten als CSV exportieren nach " + datei);
                    Console.WriteLine("ENTER beendet das Programm.");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }
            using (StreamReader reader = new StreamReader(datei))
            {
                Console.Write("Fehlzeiten aus Webuntis ".PadRight(70, '.'));

                while (true)
                {
                    string line = reader.ReadLine();
                    try
                    {
                        Fehlzeit fehlzeit = new Fehlzeit();
                        var x = line.Split('\t');
                        fehlzeit.StudentId = Convert.ToInt32(x[2]);
                        fehlzeit.Name = x[3];
                        fehlzeit.Vorname = x[4];
                        fehlzeit.Klasse = x[5];
                        fehlzeit.AbsenceHours = Convert.ToInt32(x[9]);
                        fehlzeit.AbsenceHoursNotExcused = Convert.ToInt32(x[10]);                        
                        this.Add(fehlzeit);
                    }
                    catch (Exception)
                    {
                    }

                    if (line == null)
                    {
                        break;
                    }
                }

                Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));
            }
        }        
    }
}