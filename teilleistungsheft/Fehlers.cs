﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace teilleistungsheft
{
    public class Fehlers : List<Fehler>
    {
        internal void DoppelfehehlerLöschen()
        {
            throw new NotImplementedException();
        }

        internal void Senden(Lehrers lehrers, Quartal quartal)
        {
            foreach (var lehrer in lehrers)
            {
                var fehlersDieserLehrer = (from f in Global.Fehlers where f.Adressat == lehrer.Kürzel select f).ToList();
                if (fehlersDieserLehrer.Count > 0)
                {
                    string body = @"Guten Morgen " + lehrer.Vorname + " " + lehrer.Nachname + " (" + lehrer.Kürzel + ")," +
                        "<br>" +
                        "<br>" +
                        "wie auf der Lehrerkonferenz besprochen, soll das Digitale Klassenbuch auch das bisherige Teilleistungsheft in sich aufnehmen." +
                        "<br>" +
                        "<br>" +
                        "Aufgrund von Rückmeldungen in der Lehrerkonferenz und nach Rücksprache mit befreundeten BKs ist der Modus der Dokumentation für <b>schriftliche Fächer</b> überarbeitet worden." +
                        "<br>" +
                        "<br>" +
                        "Im Folgenden wird für Ihren Unterricht aufgezeigt, was als Nächstes zu tun ist. Die verlinkten Videos zeigen das im Detail:" +
                        "<table border='1'>" +
                        "<tr>" +
                        "<th border='1px solid #dddddd' text-align='left' padding='8px'>Klasse</th>" +
                        "<th border='1px solid #dddddd' text-align='left' padding='8px'>Fach</th>" +
                        "<th border='1px solid #dddddd' text-align='left' padding='8px'>Beschreibung</th>" +
                        "</tr>";
                    
                    foreach (var fehler in fehlersDieserLehrer)
                    {
                        body += @"<tr>" +
                            "<td>" + fehler.Klasse + "</td>" +
                            "<td>" + fehler.Fach + "</td>" +
                            "<td>" + fehler.Fehlerbeschreibung + "</td>" +
                            "</tr>";                    
                    }

                    body += "</table>" +
                        "<br>" +
                        "<br>" +
                        "Falls etwas unklar oder unrichtig erscheint, dann bitte melden!" +
                        "<br>" +
                        "<br>" +
                        "Mit kollegialen Grüßen" +
                        "Stefan Bäumer";
                    
                    if (lehrer.Mail.EndsWith("@berufskolleg-borken.de") && lehrer.Kürzel != "LAT" && lehrer.Kürzel != "?")
                        Global.MailSenden(lehrer, "Dokumentation der Teilleistungen im Digitalen Klassenbuch", body);
                }
            }
        }

        internal void DateiAusgabe(string outputPfad)
        {
            using (StreamWriter outputFile = new StreamWriter(outputPfad))
            {
                foreach (var item in this)
                {
                    try
                    {
                        outputFile.WriteLine(item.Adressat.PadRight(4) + "|" + item.Klasse.PadRight(6) + "|" + item.Fehlerbeschreibung);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        Console.ReadKey();
                    }
                }
            }
            EditorOeffnen(outputPfad);
        }

        public static void EditorOeffnen(string pfad)
        {
            try
            {
                System.Diagnostics.Process.Start(@"C:\Program Files (x86)\Notepad++\Notepad++.exe", pfad);
            }
            catch (Exception)
            {
                System.Diagnostics.Process.Start("Notepad.exe", pfad);
            }
        }
    }
}