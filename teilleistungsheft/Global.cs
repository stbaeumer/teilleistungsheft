﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace teilleistungsheft
{
    internal class Global
    {
        public static EmailAddress AdminMail { get; private set; }        
        public static Fehlers Fehlers { get; internal set; }
        public static int Schulnummer { get; internal set; }

        internal static void DateiOk()
        {
            throw new NotImplementedException();
        }

        public static string SafeGetString(OleDbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return string.Empty;
        }

        internal static void MailSenden(Lehrer lehrer, string subject, string body)
        {
            ExchangeService exchangeService = new ExchangeService()
            {
                UseDefaultCredentials = true,
                TraceEnabled = false,
                TraceFlags = TraceFlags.All,
                Url = new Uri("https://ex01.bkb.local/EWS/Exchange.asmx")
            };
            EmailMessage message = new EmailMessage(exchangeService);

            message.ToRecipients.Add("stefan.baeumer@berufskolleg-borken.de");
            message.BccRecipients.Add(lehrer.Mail);

            message.Subject = subject;
            
            message.Body = body;
            
                message.Save(WellKnownFolderName.Drafts);
                //message.SendAndSaveCopy();
            
            //Console.WriteLine("................................. ok.");
            //Console.ReadKey();
        }
    }
}