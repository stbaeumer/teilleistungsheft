﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace teilleistungsheft
{
    public class Fachs : List<Fach>
    {
        public Fachs(string connectionStringUntis, string aktSjUntis)
        {
            using (OleDbConnection oleDbConnection = new OleDbConnection(connectionStringUntis))
            {
                Console.Write("Fächer aus Webuntis ".PadRight(70, '.'));

                try
                {
                    string queryString = @"SELECT DISTINCT 
Subjects.Subject_ID,
Subjects.Name,
Subjects.Longname,
Subjects.Text,
Description.Name
FROM Description RIGHT JOIN Subjects ON Description.DESCRIPTION_ID = Subjects.DESCRIPTION_ID
WHERE Subjects.Schoolyear_id = " + aktSjUntis + " AND Subjects.Deleted=No  AND ((Subjects.SCHOOL_ID)=177659) ORDER BY Subjects.Name;";

                    OleDbCommand oleDbCommand = new OleDbCommand(queryString, oleDbConnection);
                    oleDbConnection.Open();
                    OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();

                    while (oleDbDataReader.Read())
                    {
                        Fach fach = new Fach()
                        {
                            IdUntis = oleDbDataReader.GetInt32(0),
                            KürzelUntis = Global.SafeGetString(oleDbDataReader, 1),
                            Langname = Global.SafeGetString(oleDbDataReader, 2)
                        };

                        this.Add(fach);
                    };

                    Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));

                    oleDbDataReader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    oleDbConnection.Close();
                }
            }
        }        
    }
}