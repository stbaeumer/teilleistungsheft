﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace teilleistungsheft
{
    public class Schueler
    {
        public int Id { get; internal set; }
        public string Kurzname { get; internal set; }
        public int Bezugsjahr { get; internal set; }
        public string Status { get; internal set; }
        public string Grade { get; internal set; }
        public string Name { get; internal set; }
        public string Firstname { get; internal set; }
        public DateTime Birthday { get; internal set; }
        public string Telefon { get; internal set; }
        public string Mail { get; internal set; }
        public DateTime Eintrittsdatum { get; internal set; }
        public string AktuellJN { get; internal set; }
        public DateTime Austrittsdatum { get; internal set; }
        public bool Volljährig { get; internal set; }
        public string Geschlecht34 { get; internal set; }
        public string Geschlecht { get; internal set; }
        public object Betrieb { get; internal set; }

        public string generateKurzname()
        {
            return fBereinigen(Name) + "." + fBereinigen(Firstname) + "." + Id;
        }
        
        public string fBereinigen(string Textinput)
        {
            string Text = Textinput;

            Text = Text.ToLower();                          // Nur Kleinbuchstaben
            Text = fUmlauteBehandeln(Text);                 // Umlaute ersetzen


            Text = Regex.Replace(Text, "-", "_");           //  kein Minus-Zeichen
            Text = Regex.Replace(Text, ",", "_");           //  kein Komma            
            Text = Regex.Replace(Text, " ", "_");           //  kein Leerzeichen
            // Text = Regex.Replace(Text, @"[^\w]", string.Empty);   // nur Buchstaben

            Text = Regex.Replace(Text, "[^a-z]", string.Empty);   // nur Buchstaben

            Text = Text.Substring(0, Math.Min(6, Text.Length));  // Auf maximal 6 Zeichen begrenzen
            return Text;
        }

        string fUmlauteBehandeln(string Textinput)
        {
            string Text = Textinput;

            // deutsche Sonderzeichen
            Text = Regex.Replace(Text, "[æ|ä]", "ae");
            Text = Regex.Replace(Text, "[Æ|Ä]", "Ae");
            Text = Regex.Replace(Text, "[œ|ö]", "oe");
            Text = Regex.Replace(Text, "[Œ|Ö]", "Oe");
            Text = Regex.Replace(Text, "[ü]", "ue");
            Text = Regex.Replace(Text, "[Ü]", "Ue");
            Text = Regex.Replace(Text, "ß", "ss");

            // Sonderzeichen aus anderen Sprachen
            Text = Regex.Replace(Text, "[ã|à|â|á|å]", "a");
            Text = Regex.Replace(Text, "[Ã|À|Â|Á|Å]", "A");
            Text = Regex.Replace(Text, "[é|è|ê|ë]", "e");
            Text = Regex.Replace(Text, "[É|È|Ê|Ë]", "E");
            Text = Regex.Replace(Text, "[í|ì|î|ï]", "i");
            Text = Regex.Replace(Text, "[Í|Ì|Î|Ï]", "I");
            Text = Regex.Replace(Text, "[õ|ò|ó|ô]", "o");
            Text = Regex.Replace(Text, "[Õ|Ó|Ò|Ô]", "O");
            Text = Regex.Replace(Text, "[ù|ú|û|µ]", "u");
            Text = Regex.Replace(Text, "[Ú|Ù|Û]", "U");
            Text = Regex.Replace(Text, "[ý|ÿ]", "y");
            Text = Regex.Replace(Text, "[Ý]", "Y");
            Text = Regex.Replace(Text, "[ç|č]", "c");
            Text = Regex.Replace(Text, "[Ç|Č]", "C");
            Text = Regex.Replace(Text, "[Ð]", "D");
            Text = Regex.Replace(Text, "[ñ]", "n");
            Text = Regex.Replace(Text, "[Ñ]", "N");
            Text = Regex.Replace(Text, "[š]", "s");
            Text = Regex.Replace(Text, "[Š]", "S");

            return Text;

        }
    }
}