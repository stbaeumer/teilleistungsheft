﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace teilleistungsheft
{
    public class Studentgroups : List<Studentgroup>
    {
        public Studentgroups()
        {
            string datei = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\StudentgroupStudents.csv";
            
            if (!File.Exists(datei))
            {
                Console.WriteLine("Die Datei " + datei + " existiert nicht.");
                Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch auf Ihren Desktop, indem Sie");
                Console.WriteLine("1. sich als admin anmelden");
                Console.WriteLine("2. auf Administration > Export klicken");
                Console.WriteLine("3. 'Schülerguppen' als CSV exportieren nach " + datei);
                Console.WriteLine("ENTER beendet das Programm.");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
            {
                if (System.IO.File.GetLastWriteTime(datei).Date != DateTime.Now.Date)
                {
                    Console.WriteLine("Die Datei " + datei + " ist nicht von heute.");
                    Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch auf Ihren Desktop, indem Sie");
                    Console.WriteLine("1. sich als admin anmelden");
                    Console.WriteLine("2. auf Administration > Export klicken");
                    Console.WriteLine("3. 'Schülerguppen' als CSV exportieren nach " + datei);
                    Console.WriteLine("ENTER beendet das Programm.");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }

            using (StreamReader reader = new StreamReader(datei))
            {
                Console.Write("Schülergruppen aus Webuntis ".PadRight(70, '.'));

                while (true)
                {
                    string line = reader.ReadLine();
                    try
                    {
                        Studentgroup studentgroup = new Studentgroup();
                        var x = line.Split('\t');
                        studentgroup.StudentId = Convert.ToInt32(x[0]);
                        studentgroup.Name = x[1];
                        studentgroup.Forename = x[2];
                        studentgroup.StudentgroupName = x[3];
                        studentgroup.Subject = x[4];
                        studentgroup.StartDate = x[5];
                        studentgroup.EndDate = x[6];
                        studentgroup.Kurzname = studentgroup.generateKurzname();
                        this.Add(studentgroup);
                    }
                    catch (Exception)
                    {
                    }

                    if (line == null)
                    {
                        break;
                    }
                }
                Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));
            }
        }
        /*
        public void AufLeistungsdatensätzePrüfen(
            List<Leistung> leistungen, 
            ExportLesson unterricht, 
            Klasses klasses, 
            Examinations examinations,
            Quartal aktuellesQuartal,    
            string bereichsleiter,
            string klassenleiter
            )
        {
            if ((from s in this
                 where s.StudentgroupName == unterricht.Studentgroup
                 from l in leistungen
                 where l.SchlüsselExtern == s.StudentId
                 where aktuellesQuartal.Solei.Contains(l.Prüfungsart)
                 where l.Fach == unterricht.Subject
                 select s).Count() == 0)
            {
                bool istSchriftlicheArbeitAngelegt = examinations.IstSchriftlicheArbeitFürDieKlasseAngelegt(unterricht, aktuellesQuartal);
                bool istSoLeiAngelegt = examinations.SoleiImZeitraumPrüfungAngelegt(unterricht, aktuellesQuartal);

                Global.Fehlers.Add(
                    new Fehler(
                        unterricht.Lehrkraft, 
                        bereichsleiter, 
                        klassenleiter, 
                        "", 
                        unterricht.Studentgroup, 
                        unterricht.Klassen, 
                        unterricht.Subject, 
                        "",
                        istSchriftlicheArbeitAngelegt ? "Es ist zwar eine schriftliche Prüfung im Kurs " + unterricht.Studentgroup + " angelegt, aber es sind dort keine Noten eingetragen. In diesem <a href='https://google.de'>Video</a> wird gezeigt, was nun zu tun ist." : "Im Kurs " + unterricht.Studentgroup + " sind keine Noten eingetragen. In diesem <a href='https://google.de'>Video</a> wird gezeigt, was in einem Fach <b>ohne</b> schriftliche Arbeiten nun zu tun ist. Im Falles eines Faches <b>mit</b> schriftlichen Arbeiten schauen Sie dieses <a href='https://google.de'>Video</a>."));
            }
            else
            {
                foreach (var kursteilnehmer in (from s in this where s.StudentgroupName == unterricht.Studentgroup select s).ToList())
                {
                    Leistung leistung = (from l in leistungen
                                         where kursteilnehmer.StudentId == l.SchlüsselExtern
                                         where l.Fach == kursteilnehmer.Subject
                                         where aktuellesQuartal.Solei.Contains(l.Prüfungsart)
                                         select l).FirstOrDefault();

                    if (leistung == null)
                    {
                        bool istSchriftlicheArbeitAngelegt = examinations.IstSchriftlicheArbeitFürSchülerAngelegt(unterricht, aktuellesQuartal, kursteilnehmer);
                        bool istSoLeiAngelegt = examinations.SoleiImZeitraumPrüfungAngelegt(unterricht, aktuellesQuartal);

                        Global.Fehlers.Add(
                            new Fehler(
                                unterricht.Lehrkraft, 
                                bereichsleiter, 
                                klassenleiter, 
                                kursteilnehmer.Name + " " + kursteilnehmer.Forename, 
                                unterricht.Studentgroup, 
                                unterricht.Klassen, 
                                unterricht.Subject, 
                                "",
                                istSchriftlicheArbeitAngelegt ? "Im Kurs " + unterricht.Studentgroup + " ist zwar eine schriftliche Prüfung angelegt, aber es sind dort keine Noten eingetragen. In diesem <a href='https://google.de'>Video</a> wird gezeigt, was nun zu tun ist." : "Im Kurs " + unterricht.Studentgroup + " sind keine Note eingetragen. In diesem <a href='https://google.de'>Video</a> wird gezeigt, was nun in einem Fach <b>ohne</b> schriftlichen Arbeiten zu tun ist. Im Falle eines Faches <b>mit</b> schriftlichen Arbeiten gubt dieses <a href='https://google.de'>Video</a> Hilfestellung."
                            )
                        );
                    }

                    // Falls ein Leistungsdatensatz existiert (gilt nicht für Zeugnisse), ...

                    if (leistung != null && !aktuellesQuartal.Id.EndsWith("Z"))
                    {
                        // ... und ein schriftliches Fach vorliegt

                        if (aktuellesQuartal.SchriftlicheArbeiten.Contains(leistung.Prüfungsart))
                        {
                            // ... muss eine Note ...

                            if (leistung.Note != "")
                            {
                                // ... und eine Bemerkung ausgefüllt sein.

                                if (leistung.Bemerkung == "")
                                {
                                    Global.Fehlers.Add(
                                        new Fehler(
                                            unterricht.Lehrkraft, 
                                            bereichsleiter, 
                                            klassenleiter, 
                                            kursteilnehmer.Name + " " + kursteilnehmer.Forename, 
                                            unterricht.Studentgroup, 
                                            leistung.Klasse, 
                                            unterricht.Subject, 
                                            leistung.Prüfungsart, 
                                            "Sie haben eine Note für die schriftliche Arbeit (" + leistung.Prüfungsart + " im Fach " + unterricht.Subject + ") eingetragen. Das ist gut. Es fehlt noch die SoLei-Note. Die muss in die Bemerkung hinter die schriftliche Note eingetragen werden. In diesem <a href='https://google.de'>Video</a> wird gezeigt, was nun zu tun ist."));
                                }
                            }
                        }
                    }
                }
            }
        }*/
    }
}