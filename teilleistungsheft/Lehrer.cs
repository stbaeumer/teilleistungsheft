﻿using System;
using System.Linq;
using System.Reflection;

namespace teilleistungsheft
{
    public class Lehrer
    {
        public int IdUntis { get; internal set; }
        public string Kürzel { get; internal set; }
        public string Mail { get; internal set; }
        public string Nachname { get; internal set; }
        public string Vorname { get; internal set; }

        internal void Mailen(string v, Quartal aktuellesQuartal)
        {
            try
            {
                string body = "Hallo " + Vorname + " " + Nachname + " (" + Kürzel + "),";
                body += "<br>";
                body += "<br>";
                body += "diese Mail wurde automatisch erstellt und erreicht Sie in Ihrer Eigenschaft als " + v + " der unten angegebenen Klassen.";
                body += "<br>";
                body += "<br>";
                body += aktuellesQuartal.Text;
                body += "<br><br>";
                body += "<table border='1'><tr><td>Klasse</td><td>Kl.Leh.</td><td>Leh.</td><td>Fach</td><td>Sch.</td><td>Das ist zu tun</td></tr>";

                int i = 0;

                var relFeh = new Fehlers();

                foreach (var fehler in (from f in Global.Fehlers where typeof(Fehler).GetProperty(v).GetValue(f).ToString() == this.Kürzel select f).ToList().OrderBy(x => x.Adressat).OrderBy(x => x.Klasse))
                {
                    body += "<tr><td>" + fehler.Klasse + "</td><td>" + fehler.Klassenleitung + "</td><td>" + fehler.Adressat + "</td><td>" + fehler.Fach + "</td><td>" + fehler.SchuelerName + "</td><td>" + fehler.Fehlerbeschreibung + "</td></tr> ";
                    i++;
                }
                Console.Write(((" " + i.ToString()).PadLeft(5, '.') + " Zeilen ").PadRight(13, '.'));
                body += "</table>";
                body += "<br>";
                body += "Haben Sie Fragen oder erscheint etwas unklar oder unrichtig? Dann bitte zeitnah melden, damit das geprüft werden kann. Unterstützung finden Sie <a href='https://bk-borken.lms.schulon.org/course/view.php?id=415'>hier</a>. Alternativ können Sie auch mailen an <a href='mailto:klassenbuch@berufskolleg-borken.de'>klassenbuch@berufskolleg-borken.de</a> oder Sie melden sich persönlich.";                
                body += "<br><br>";
                body += "Vielen Dank für Ihre Unterstützung!";
                body += "<br><br>";                
                body += "Stefan Bäumer";
                body += "<br><br>";
                

                Global.MailSenden(this, aktuellesQuartal.Betreff + "  (" + v + ": " + Kürzel + ")", body);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadKey();
            }
        }
    }
}