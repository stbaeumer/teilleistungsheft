﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace teilleistungsheft
{
    public class Leistung
    {
        public DateTime Datum { get; internal set; }
        public string Name { get; internal set; }
        public string Klasse { get; internal set; }
        public string Fach { get; internal set; }
        public string Prüfungsart { get; internal set; }
        public string Note { get; internal set; }
        public string Bemerkung { get; internal set; }
        public string Benutzer { get; internal set; }
        public int SchlüsselExtern { get; internal set; }
        public int LeistungId { get; internal set; }
        public bool ReligionAbgewählt { get; internal set; }

        internal void AufSoleiPrüfen(
            Schueler schueler, 
            string klasse, 
            ExportLesson unterricht, 
            Klasses klasses, 
            Quartal aktuellesQuartal)        
        {            
            if (this != null)
            {
                // ... und ein schriftliches Fach vorliegt

                if (aktuellesQuartal.SchriftlicheArbeiten.Contains(this.Prüfungsart))
                {
                    // ... muss eine Note ...

                    if (this.Note != "")
                    {
                        // ... und eine Bemerkung ausgefüllt sein.

                        if (this.Bemerkung == "")
                        {
                            var bereichsleiter = 
                                (
                                    from k in klasses
                                    where k.NameUntis == klasse
                                    select k.Abteilung
                                ).FirstOrDefault();

                            var klassenleiter = 
                                (
                                    from k in klasses
                                    where k.NameUntis == klasse
                                    select k.KlassenlehrerKürzel
                                ).FirstOrDefault();

                            /*Global.Fehlers.Add(
                                new Fehler(
                                    unterricht.Lehrkraft, 
                                    bereichsleiter,klassenleiter, 
                                    schueler.Name + " " + schueler.Firstname, 
                                    unterricht.Studentgroup, 
                                    klasse, 
                                    unterricht.Subject, 
                                    "",
                                    "Sie haben eine schriftliche Prüfung (" + Prüfungsart + " im Fach " + unterricht.Subject + ") angelegt und eine Note eingetragen. Zu jeder schriftlichen Arbeit muss es eine SoLei-Note geben. Die muss in die Bemerkung hinter die schriftliche Note eingetragen werden. In diesem <a href='https://google.de'>Video</a> wird gezeigt, wie es geht."));*/
                        }
                    }
                }
            }
        }
    }
}