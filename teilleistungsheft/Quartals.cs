﻿using System;
using System.Collections.Generic;

namespace teilleistungsheft
{
    public class Quartals : List<Quartal>
    {
        public Quartals()
        {
            int sj = (DateTime.Now.Month >= 8 ? DateTime.Now.Year : DateTime.Now.Year - 1);

            this.Add(
                new Quartal(
                    "1", 
                    new DateTime(sj, 08, 01), 
                    new DateTime(sj, 11, 15), 
                    "https://google.de", 
                    "https://google.de", 
                    new List<string>() { "Q1 - SoLei" },
                    new List<string>() { "Klausur (Anlage D)", "Klassenarbeit (Anlage A-C)", "Klausur (Gym.)" },
                    "Dokumentation der Teilleistungen des 1.Quartals",
                    "Es geht um die Dokumentation der Teilleistungen des 1.Quartals. <br><br> Die folgende Tabelle zeigt, wo Handlungsbedarf besteht. Bitte helfen Sie mit, dass das Teilleistungsheft für das 1.Quartal in den nächsten 14 Tage abgeschlossen werden kann."
                )
            );
            this.Add(
                new Quartal(
                    "2", 
                    new DateTime(sj, 11, 15), 
                    new DateTime(sj + 1, 1, 31), 
                    "https://google.de", 
                    "https://google.de", 
                    new List<string>() { "Klassenarbeit (Anlage A-C)", "Klausur (Anlage D)", "Q1-SoLei", "Klausur (Gym.)" }, 
                    new List<string>() { "Klausur (Anlage D)", "Klassenarbeit (Anlage A-C)", "Klausur (Gym.)" },
                    "Dokumentation der Teilleistungen des 2.Quartals",
                    "Es geht um die Dokumentation der Teilleistungen des 2.Quartals. <br><br> Im Folgenden werden alle fehlenden Einträge aufgelistet. Bitte helfen Sie mit, dass das Teilleistungsheft für das 2.Quartal in den nächsten 14 Tage abgeschlossen werden kann."
                 )
            );
            this.Add(
                new Quartal(
                    "3", 
                    new DateTime(sj + 1, 02, 01), 
                    new DateTime(sj + 1, 03, 31), 
                    "https://google.de", 
                    "https://google.de´", 
                    new List<string>() { "Klassenarbeit(Anlage A-C)", "Klausur(Anlage D)", "Q1 - SoLei", "Klausur (Gym.)" }, 
                    new List<string>() { "Klausur (Anlage D)", "Klassenarbeit (Anlage A-C)", "Klausur (Gym.)" },
                    "Dokumentation der Teilleistungen des 3.Quartals",
                    "Es geht um die Dokumentation der Teilleistungen des 3.Quartals. <br><br> Im Folgenden werden alle fehlenden Einträge aufgelistet. Bitte helfen Sie mit, dass das Teilleistungsheft für das 3.Quartal in den nächsten 14 Tage abgeschlossen werden kann."
                )
            );
            this.Add(
                new Quartal(
                    "4", 
                    new DateTime(sj + 1, 04, 01), 
                    new DateTime(sj + 1, 7, 1), 
                    "https://google.de", 
                    "https://google.de", 
                    new List<string>() { "Klassenarbeit (Anlage A-C)", "Klausur (Anlage D)", "Q1-SoLei", "Klausur (Gym.)" }, 
                    new List<string>() { "Klausur (Anlage D)", "Klassenarbeit (Anlage A-C)", "Klausur (Gym.)" },
                    "Dokumentation der Teilleistungen des 1.Quartals",
                    "Es geht um die Dokumentation der Teilleistungen des 4.Quartals. <br><br> Im Folgenden werden alle fehlenden Einträge aufgelistet. Bitte helfen Sie mit, dass das Teilleistungsheft für das 4.Quartal in den nächsten 14 Tage abgeschlossen werden kann."
                )
            );
            this.Add(
                new Quartal(
                    "HZ", 
                    new DateTime(sj, 08, 01), 
                    new DateTime(sj + 1, 7, 1), 
                    "https://google.de", 
                    "https://google.de", 
                    new List<string>() { "Halbjahreszeugnis" }, 
                    new List<string>() { "Halbjahreszeugnis" },
                    "Erfassung der Halbjahreszeugnisnoten für die automatische Übergabe an das Zeugnisprogramm",
                    "Es geht um die Erfassung der Zeugnisnoten für das Halbjahreszeugnis. Im Gegensatz zu früheren Jahren müssen Sie die Noten nun einmalig selbst erfassen, damit die Noten anschließend direkt auf die Halbjahrszeugnisse übertragen werden können. <br><br> Im Folgenden werden alle fehlenden Einträge aufgelistet. Bitte helfen Sie mit, dass die Halbjahresnoten am Tag der Konferenz vollständig vorliegen. <br><br> Kurz zusammengefasst müssen Sie für jeden Unterricht eine Prüfung der Prüfungsart '<b>Halbjahreszeugnis</b>' anlegen und dort dann Noten für jeden Teilnehmer eintragen."
                )
            );
            this.Add(
                new Quartal(
                    "JZ", 
                    new DateTime(sj, 08, 01), 
                    new DateTime(sj + 1, 7, 1), 
                    "https://google.de", 
                    "https://google.de", 
                    new List<string>() { "Jahreszeugnis" }, 
                    new List<string>() { "Jahreszeugnis" },
                    "Erfassung der Jahreszeugnisnoten für die automatische Übergabe an das Zeugnisprogramm",
                    "Es geht um die Erfassung der Zeugnisnoten für das Jahreszeugnis. Im Gegensatz zu früheren Jahren müssen Sie die Noten nun einmalig selbst erfassen, damit die Noten anschließend direkt auf die Jahrszeugnisse übertragen werden können. <br><br> Im Folgenden werden alle fehlenden Einträge aufgelistet. Bitte helfen Sie mit, dass die Jahrenoten am Tag der Konferenz vollständig vorliegen. "
                )
            );
        }

        public Quartal GetAktuellesQuartal(string quartal)
        {
            if (quartal == "HZ")
                return this[4];
            if(quartal == "JZ")
                return this[5];
            return this[Convert.ToInt32(quartal) - 1];
        }
    }
}