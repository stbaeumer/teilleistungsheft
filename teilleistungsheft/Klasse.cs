﻿using System.Collections.Generic;

namespace teilleistungsheft
{
    public class Klasse
    {
        public int IdUntis { get; internal set; }
        public string NameUntis { get; internal set; }
        public string LangNameUntis { get; internal set; }
        public string KlassenlehrerKürzel { get; internal set; }
        public string SchildJahrgang { get; internal set; }
        public List<ExportLesson> Unterrichts { get; internal set; }
        public string Abteilung { get; internal set; }
    }
}