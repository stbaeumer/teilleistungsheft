﻿namespace teilleistungsheft
{
    public class Fach
    {
        public Fach()
        {
        }

        public int IdUntis { get; set; }
        public string KürzelUntis { get; set; }
        public string Langname { get; internal set; }
    }
}