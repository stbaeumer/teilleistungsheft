﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace teilleistungsheft
{
    public class Leistungen : List<Leistung>
    {
        public Leistungen()
        {
        }

        public Leistungen(string datei)
        {
            if (!File.Exists(datei))
            {
                Console.WriteLine("Die Datei " + datei + " existiert nicht.");
                Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
                Console.WriteLine("1. sich als admin anmelden");
                Console.WriteLine("2. auf Klassenbuch > Berichte klicken");
                Console.WriteLine("3. Noten pro Schüler als CSV exportieren nach " + datei);
                Console.WriteLine("ENTER beendet das Programm.");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
            {
                if (System.IO.File.GetLastWriteTime(datei).Date != DateTime.Now.Date)
                {
                    Console.WriteLine("Die Datei " + datei + " ist nicht von heute.");
                    Console.WriteLine("Exportieren Sie die Datei aus dem Digitalen Klassenbuch, indem Sie");
                    Console.WriteLine("1. sich als admin anmelden");
                    Console.WriteLine("2. auf Klassenbuch > Berichte klicken");
                    Console.WriteLine("3. Noten pro Schüler als CSV exportieren nach " + datei);
                    Console.WriteLine("ENTER beendet das Programm.");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }

            using (StreamReader reader = new StreamReader(datei))
            {
                string überschrift = reader.ReadLine();

                Console.Write("Leistungsdaten aus Webuntis ".PadRight(70, '.'));

                while (true)
                {
                    string line = reader.ReadLine();

                    try
                    {
                        if (line != null)
                        {
                            Leistung leistung = new Leistung();
                            var x = line.Split('\t');

                            leistung.Datum = DateTime.ParseExact(x[0], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            leistung.Name = x[1];
                            leistung.Klasse = x[2];
                            leistung.Fach = x[3];
                            leistung.Prüfungsart = x[4];
                            leistung.Note = x[5];
                            leistung.Bemerkung = x[6];
                            leistung.Benutzer = x[7];
                            leistung.SchlüsselExtern = Convert.ToInt32(x[8]);
                            this.Add(leistung);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    if (line == null)
                    {
                        break;
                    }
                }
                Console.WriteLine((" " + this.Count.ToString()).PadLeft(30, '.'));
            }
        }

        public List<Leistung> BerwerteteSchriftlicheArbeiten(ExportLesson unterricht, Quartal aktuellesQuartal)
        {
            return (from l in this
                    where unterricht.Klassen.Contains(l.Klasse)
                    where unterricht.Subject == l.Fach
                    where (aktuellesQuartal.Von <= l.Datum && l.Datum <= aktuellesQuartal.Bis)
                    where aktuellesQuartal.SchriftlicheArbeiten.Contains(l.Prüfungsart)
                    select l).ToList();
        }
        
        internal List<Leistung> BerwerteteSonstigeLeistungen(ExportLesson unterricht, Quartal aktuellesQuartal)
        {
            return (from l in this
                    where unterricht.Klassen.Contains(l.Klasse)
                    where unterricht.Subject == l.Fach
                    where (aktuellesQuartal.Von <= l.Datum && l.Datum <= aktuellesQuartal.Bis)
                    where aktuellesQuartal.Solei.Contains(l.Prüfungsart)
                    select l).ToList();
        }

        internal List<Leistung> KorrespondierendeSonstigeLeistungen(ExportLesson unterricht, Quartal aktuellesQuartal)
        {
            return (from l in this
                    where unterricht.Klassen.Contains(l.Klasse)
                    where unterricht.Subject == l.Fach
                    where (aktuellesQuartal.Von <= l.Datum && l.Datum <= aktuellesQuartal.Bis)
                    where aktuellesQuartal.SchriftlicheArbeiten.Contains(l.Prüfungsart)
                    where l.Bemerkung != ""
                    select l).ToList();
        }
    }
}