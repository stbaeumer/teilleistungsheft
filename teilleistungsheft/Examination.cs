﻿using System;

namespace teilleistungsheft
{
    public class Examination
    {
        public string Prüfungsart { get; internal set; }
        public string Name { get; internal set; }
        public string Klasse { get; internal set; }
        public string Note { get; internal set; }
        public DateTime Datum { get; internal set; }
        public DateTime Von { get; internal set; }
        public DateTime Bis { get; internal set; }
        public DateTime Rückgabe { get; internal set; }
        public string Fach { get; internal set; }
        public string Lehrer { get; internal set; }
        public int Foreignkey { get; internal set; }
        public Fach Fachlangname { get; internal set; }
    }
}